import { ApiRequest } from "../request";

const baseUrl:string = "http://tasque.lol/";

export class RegisterController {
    
    async createUser(dataset) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body(dataset)
            .send();
        return response;
    }
}