import { ApiRequest } from "../request";

const baseUrl: string = "http://tasque.lol/";

export class CommentsController {
    async postComment(testData, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body(testData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
