import { ApiRequest } from "../request";

const baseUrl: string = "http://tasque.lol/";

export class AuthController {
    async loginWithData(dataset) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Auth/login`)
            .body(dataset)
            .send();
        return response;
    }

    async loginWithSet(email, password) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Auth/login`)
            .body({
                email: email,
                password: password,
            })
            .send();
        return response;
    }
}
