import { ApiRequest } from "../request";

const baseUrl: string = "http://tasque.lol/";

export class PostController {
    async postPost(testData, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body(testData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async postLike(testData, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body(testData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
