import { ApiRequest } from "../request";

const baseUrl: string = "http://tasque.lol/";

export class UsersController {
    async getAllUsers() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method("GET").url(`api/Users`).send();
        return response;
    }

    async getUserFromToken(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users/fromToken`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async updateUser(userData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("PUT")
            .url(`api/Users`)
            .body(userData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async getUserById(id: number) {
        const response = await new ApiRequest().prefixUrl(baseUrl).method("GET").url(`api/Users/${id}`).send();
        return response;
    }

    async deleteUserById(id: number, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("DELETE")
            .url(`api/Users/${id}`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
