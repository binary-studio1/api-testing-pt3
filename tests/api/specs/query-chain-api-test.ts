import { expect } from "chai";
import { RegisterController } from "../lib/controllers/register.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode } from "../../helpers/functionForChecling.helper";
import { checkUserName } from "../../helpers/functionForChecling.helper";

const users = new UsersController();
const auth = new AuthController();
const regist = new RegisterController();

//const schemas = require('./data/schemas_testData.json');
//const chai = require('chai');
//chai.use(require('chai-json-schema'));

describe(`First chain`, () => {
    let accessToken: string, id: number, prevUserName: string;
    let validRegistrationSet = {
        email: "valeriaserdiuck@gmail.com",
        userName: "spoyler",
        password: "12345678",
    };

    it(`Register a new user`, async () => {
        let response = await regist.createUser(validRegistrationSet);

        //console.log(response.statusCode);
        checkStatusCode(response, 201);
    });

    it(`Return all users`, async () => {
        let response = await users.getAllUsers();

        // console.log("All Users:");
        // console.log(response.body);
        checkStatusCode(response, 200);
    });

    it(`Login and get the token`, async () => {
        let response = await auth.loginWithData(validRegistrationSet);
        id = response.body.user.id;
        prevUserName = response.body.user.userName;
        accessToken = response.body.token.accessToken.token;
        checkStatusCode(response, 200);
        //console.log(accessToken);
    });

    it(`Find current user`, async () => {
        let response = await users.getUserFromToken(accessToken);
        //console.log(response.body);
    });

    it(`Update user`, async () => {
        let validCredentialDataSet = {
            id: id,
            userName: prevUserName,
        };
        let response = await users.updateUser(validCredentialDataSet, accessToken);
        checkStatusCode(response, 204);
        //console.log(response.body);
    });

    it(`Ensure that inf was updated`, async () => {
        let response = await users.getUserFromToken(accessToken);
        //console.log(response.body);
        checkUserName(response, validRegistrationSet);
    });

    it(`Find user by id`, async () => {
        let response = await users.getUserById(id);
        checkStatusCode(response, 200);
    });

    it(`Delete user by id`, async () => {
        let response = await users.deleteUserById(id, accessToken);
        checkStatusCode(response, 204);
    });

    after(`Check the user cannot be found with invalid id`, async () => {
        let response = await users.getUserById(id);
        checkStatusCode(response, 404);
    });
});
