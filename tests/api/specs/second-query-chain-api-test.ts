import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { CommentsController } from "../lib/controllers/comments.controller";
import { PostController } from "../lib/controllers/post.controller";
import { checkStatusCode } from "../../helpers/functionForChecling.helper";

const users = new UsersController();
const auth = new AuthController();
const comment = new CommentsController();
const post = new PostController();

//const schemas = require('./data/schemas_testData.json');
//const chai = require('chai');
//chai.use(require('chai-json-schema'));

describe(`Second chain`, () => {
    let postId: number,
        userEmail = "valeriaserdiuck@gmail.com",
        userName = "spoyler",
        userPassword = "12345678",
        accessToken: string,
        id: number;

    it(`Return all users`, async () => {
        let response = await users.getAllUsers();

        // console.log("All Users:");
        // console.log(response.body);
        checkStatusCode(response, 200);
    });

    before(`Login and get the token`, async () => {
        let response = await auth.loginWithSet(userEmail, userPassword);
        id = response.body.user.id;
        accessToken = response.body.token.accessToken.token;
        checkStatusCode(response, 200);
        //console.log(accessToken);
    });

    it(`Find user by id`, async () => {
        let response = await users.getUserById(id);
        checkStatusCode(response, 200);
    });

    it(`Check the user cannot be found by invalid id`, async () => {
        let response = await users.getUserById(0);
        checkStatusCode(response, 404);
    });

    it(`Publish the post`, async () => {
        let response = await post.postPost(
            {
                authorId: id,
                body: "test post",
            },
            accessToken
        );
        postId = response.body.id;
        checkStatusCode(response, 200);
    });

    it(`Post comment to the published post`, async () => {
        let response = await comment.postComment(
            {
                authorId: id,
                postId: postId,
                body: "test comm",
            },
            accessToken
        );
        checkStatusCode(response, 200);
    });

    it(`Like post`, async () => {
        let response = await post.postLike(
            {
                entityId: postId,
                isLike: true,
                userId: id,
            },
            accessToken
        );
        checkStatusCode(response, 200);
    });

    it(`Check the post is not liked without authorization`, async () => {
        let response = await post.postLike(
            {
                entityId: postId,
                isLike: true,
                userId: id,
            },
            "123"
        );
        checkStatusCode(response, 401);
    });
});
