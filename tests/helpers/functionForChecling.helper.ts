import { expect } from "chai";

export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(statusCode);
}

export function checkUserName(response, prevUserName) {
    expect(response.body.userName, `The new username should not match to previous`).not.to.equal(prevUserName);
}